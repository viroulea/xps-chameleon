#!/bin/bash

starpu_version="1.2.6"
chameleon_url="git@gitlab.inria.fr:solverstack/chameleon.git"
chameleon_version="7a5f42b5440ef63dd655d0bc8d38abcc98be198c"
host=`hostname`
download_dir=$HOME
chameleon_dir=$HOME/chameleon
starpu_install_dir=$HOME/install-${host}/starpu-${starpu_version}

cd $download_dir
if [ ! -d "chameleon" ]; then
  git clone --recursive $chameleon_url
fi

cd chameleon
git checkout $chameleon_version
git submodule update

# Patch for chameleon to make the output more friendly
PATCH_CHAMELEON=$(cat <<'END_PATCH'
diff --git a/timing/timing.c b/timing/timing.c
index 8ce10c99..e5b1b79b 100644
--- a/timing/timing.c
+++ b/timing/timing.c
@@ -141,8 +141,11 @@ Test(int64_t n, int *iparam) {
         return 0;
     }
 
-    if ( CHAMELEON_My_Mpi_Rank() == 0)
-        printf( "%7d %7d %7d ", iparam[IPARAM_M], iparam[IPARAM_N], iparam[IPARAM_K] );
+    if ( CHAMELEON_My_Mpi_Rank() == 0 ) {
+        printf( "%10d %7d %7d %7d %7d ", iparam[IPARAM_THRDNBR],
+                iparam[IPARAM_M], iparam[IPARAM_N],
+                iparam[IPARAM_NB], iparam[IPARAM_K] );
+    }
     fflush( stdout );
 
     t = (double*)malloc(niter*sizeof(double));
@@ -423,7 +426,7 @@ print_header(char *prog_name, int * iparam) {
             iparam[IPARAM_IB],
             eps );
 
-    printf( "#     M       N  K/NRHS   seconds   Gflop/s Deviation%s%s\n",
+    printf( "#  Threads       M       N      NB  K/NRHS   seconds   Gflop/s Deviation%s%s\n",
             bound_header, iparam[IPARAM_INVERSE] ? inverse_header : check_header);
     return;
 }
END_PATCH
)

if [ "$chameleon_version" != "7a5f42b5440ef63dd655d0bc8d38abcc98be198c" ]; then
  # Patch is only valid for versions after this hash
  echo "$PATCH_CHAMELEON" > chameleon.patch
  patch -p1 < chameleon.patch
fi

# We want to build both OpenMP and StarPU, for the specific -current- host
build_dir_openmp=$chameleon_dir/build-${host}-libgomp
build_dir_starpu=$chameleon_dir/build-${host}-starpu-${starpu_version}

echo "Configuring Chameleon+OpenMP"
if [ ! -d $build_dir_openmp ]; then
  mkdir -p $build_dir_openmp
fi
cd $build_dir_openmp
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_Fortran_COMPILER=gfortran -DCHAMELEON_SCHED=OPENMP

echo "Building Chameleon+OpenMP"
make -j8

echo "Configuring Chameleon+StarPU"
if [ ! -d $build_dir_starpu ]; then
  mkdir -p $build_dir_starpu
fi

cd $build_dir_starpu
cmake .. -DCMAKE_BUILD_TYPE=Release -DCHAMELEON_USE_CUDA=OFF -DCHAMELEON_USE_MPI=OFF -DSTARPU_DIR=$starpu_install_dir -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_Fortran_COMPILER=gfortran

echo "Building Chameleon+StarPU"
make -j8
