#!/bin/bash
host=`hostname`
plasma_version="af74518fa451"
plasma_url="https://bitbucket.org/icl/plasma/get/${plasma_version}.zip"
# Plasma needs a specific lua to build, it's likely both versions are strongly tied.
lua_version="5.3.4"
lua_url="https://www.lua.org/ftp/lua-${lua_version}.tar.gz"

download_dir=$HOME/plasmas
# No out of tree build for plasma :(
build_dir=$HOME/plasmas/plasma-${plasma_version}-${host}-libgomp

# Patch for plasma to make the output more friendly
PATCH_PLASMA=$(cat <<'END_PATCH'
diff -r 43a7ec5062bf test/test.c
--- a/test/test.c	Thu Aug 09 14:58:45 2018 -0400
+++ b/test/test.c	Fri Nov 09 11:51:59 2018 +0100
@@ -15,6 +15,7 @@
 #include <stdlib.h>
 #include <string.h>
 #include <stdbool.h>
+#include <omp.h>
 
 /******************************************************************************/
 typedef void (*test_func_ptr)(param_value_t param[], bool run);
@@ -315,6 +316,9 @@
     {"ortho",              "Orth. error",  9,     false,
      "orthogonality error"},
 
+    {"threads",               "Threads",         9,     false,
+     "Number of threads used"},
+
     {"time",               "Time",         9,     false,
      "time to solution"},
 
@@ -540,6 +544,7 @@
             case PARAM_ERROR:
             case PARAM_ORTHO:
             case PARAM_TIME:
+            case PARAM_THREADS:
             case PARAM_GFLOPS:
             case PARAM_ITERSV:
                 break;
@@ -673,6 +678,7 @@
             case PARAM_ZEROCOL:
             case PARAM_INCX:
             case PARAM_ITERSV:
+            case PARAM_THREADS:
                 printf("  %*d", ParamDesc[i].width, pval[i].i);
                 break;
 
@@ -715,7 +721,11 @@
     pval[PARAM_SUCCESS].used = true;
     pval[PARAM_ERROR  ].used = true;
     pval[PARAM_TIME   ].used = true;
+    pval[PARAM_THREADS].used = true;
     pval[PARAM_GFLOPS ].used = true;
+#pragma omp parallel
+#pragma omp master
+    pval[PARAM_THREADS].i = omp_get_num_threads();
 
     bool found = false;
     for (int i = 0; routines[i].name != NULL; ++i) {
diff -r 43a7ec5062bf test/test.h
--- a/test/test.h	Thu Aug 09 14:58:45 2018 -0400
+++ b/test/test.h	Fri Nov 09 11:51:59 2018 +0100
@@ -25,6 +25,7 @@
     PARAM_SUCCESS, // success indicator
     PARAM_ERROR,   // numerical error
     PARAM_ORTHO,   // orthogonality error
+    PARAM_THREADS, // threads used
     PARAM_TIME,    // time to solution
     PARAM_GFLOPS,  // GFLOPS rate
     PARAM_ITERSV,  // iterations to solution
END_PATCH
)

# Our make.inc for plasma

MAKE_INC_PLASMA=$(cat <<'END_PATCH'
# PLASMA example make.inc, using Intel MKL and gcc
#
# PLASMA is a software package provided by:
# University of Tennessee, US,
# University of Manchester, UK.

# --------------------
# programs

CC        = gcc
FC        = gfortran
AR        = ar
RANLIB    = ranlib


# --------------------
# flags

# Use -fPIC to make shared (.so) and static (.a) libraries;
# can be commented out if making only static libraries.
FPIC      = -fPIC

CFLAGS    = -fopenmp $(FPIC) -O3 -std=c99   -Wall -pedantic -Wshadow -Wno-unused-function
FCFLAGS   = -fopenmp $(FPIC) -O3 -std=f2008 -Wall
LDFLAGS   = -fopenmp $(FPIC)

# options for MKL
CFLAGS   += -DHAVE_MKL

# one of: aix bsd c89 freebsd generic linux macosx mingw posix solaris
# usually generic is fine
lua_platform = linux

# --------------------
# PLASMA is a library in C, but can be also used from Fortran.
# In this case, Fortran interface needs to be build.
# 0 = no  - Fortran codes will not be touched, the FC and FCFLAGS variables
#           will not be referenced
# 1 = yes - Fortran interface will be compiled and included into the library

fortran ?= 1

# --------------------
# libraries
# This assumes $MKLROOT is set in your environment.
# Add these to your .cshrc or .bashrc, adjusting for where MKL is installed:
# in .cshrc:   source /opt/intel/bin/compilervars.csh intel64
# in .bashrc:  source /opt/intel/bin/compilervars.sh  intel64

# With gcc OpenMP (libgomp), use -lmkl_sequential or (-lmkl_gnu_thread   with MKL_NUM_THREADS=1).
# With icc OpenMP (liomp5),  use -lmkl_sequential or (-lmkl_intel_thread with MKL_NUM_THREADS=1).
LIBS      = -L$(MKLROOT)/lib -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lm -ldl

INC       = -I$(MKLROOT)/include
END_PATCH
)


cd $download_dir
if [ ! -d $build_dir ]; then
  echo "Getting plasma"
  wget $plasma_url
  echo "Extracting plasma"
  unzip ${plasma_version}.zip
  echo "Moving plasma to $build_dir"
  mv icl-plasma-${plasma_version} $build_dir
  echo "Cleanup tgz"
  rm -f ${plasma_version}.zip
  cd $build_dir
  echo "$MAKE_INC_PLASMA" > make.inc
  echo "$PATCH_PLASMA" > plasma.patch
  echo "Patching plasma"
  patch -p1 < plasma.patch
fi
cd $build_dir

echo "Building plasma"
make test -j8

