#!/bin/bash

version="1.2.6"
host=`hostname`

install_dir=$HOME/install-${host}/starpu-$version

download_dir=$HOME

release_file="http://starpu.gforge.inria.fr/files/starpu-${version}/starpu-${version}.tar.gz"

echo "Getting StarPU"
cd $download_dir
wget $release_file
tar -xf "starpu-${version}.tar.gz"
cd starpu-$version
mkdir build-$host && cd build-$host
echo "Configuring StarPU"
../configure --prefix=$install_dir CC=gcc CXX=g++ F77=gfortran FC=gfortran --enable-maxcpus=96
echo "Building and installing StarPU"
make -j16 install
