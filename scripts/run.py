#!/usr/bin/env python
# -*- coding: utf-8 -*-
import itertools
import argparse
from time import gmtime, strftime
import subprocess
import glob
import sys
import os

OPTIONS_BY_ARCH={
    "souris": {
      "blocksizes": [128, 192, 224, 256, 320, 384, 448, 512],
      "matrix_sizes": [(8192, 320), (16384, 320), (32768, 320)],
      "threads": [96, 88, 80, 72, 64, 48, 32, 16, 8, 4],
      },
    "brise": {
      "blocksizes": [128, 192, 224, 256, 320, 384, 448, 512],
      "matrix_sizes": [(8192, 320), (16384, 320), (32768, 320)],
      "threads": [96, 88, 80, 72, 64, 48, 32, 16, 8, 4],
      },
    }

OPTIONS_BY_LIB={
  "chameleon": {
    "progs": [
      # "dpotrf_tile",
      # "dgemm_tile",
      # "dgeqrf_tile",
      # "dgetrf_nopiv_tile",
      "dpotrf",
      "dgemm",
      "dgeqrf",
      "dgetrf_nopiv",
      ],
    "command_template": "time_%(prog)s",
    "runtimes": ["starpu-1.2.6", "libgomp"],
    "args": "-m %(n)s -n %(n)s -k %(n)s -b %(bs)s --niter=%(iters)s",
    "path": "/home/pviroule/chameleon/build-%(ARCH)s-%(RUNTIME)s/timing",
    },
  "plasma": {
    "progs": [
      "dpotrf",
      "dgemm",
      "dtrsm",
      "dgetrf",
      "dgeqrf",
      ],
    "command_template": "test %(prog)s --test=n",
    "runtimes": ["libgomp"],
    "args": "--dim=%(n)s --nb=%(bs)s --iter=%(iters)s",
    "path": "/home/pviroule/plasmas/plasma-af74518fa451-%(ARCH)s-%(RUNTIME)s/test",
    }
}

OPTIONS_BY_RUNTIME={
    "starpu": {
      "env_vars": "STARPU_NCPU=%(threads)s",
      },
    "libgomp": {
      "env_vars": "OMP_NUM_THREADS=%(threads)s OMP_PLACES=\"{0}:%(threads)s:1\""
      },
    }

OUTFILE="/home/pviroule/xps-chameleon/logs/%(date)s.%(arch)s.%(lib)s.%(runtime)s.%(prog)s.%(numactl)s.log"
TIME_FORMAT="%Y-%m-%d_%H:%M:%S"
DATETIME = strftime(TIME_FORMAT, gmtime())

def get_cmds_for(thread, lib, arch, n, bs, numactl, iters):
  commands = []
  for runtime in OPTIONS_BY_LIB[lib]["runtimes"]:
    path = OPTIONS_BY_LIB[lib]["path"] % { 'ARCH': arch, 'RUNTIME': runtime }
    if runtime.startswith("starpu"):
      env_vars = OPTIONS_BY_RUNTIME["starpu"]["env_vars"]
    elif runtime.startswith("libgomp"):
      env_vars = OPTIONS_BY_RUNTIME["libgomp"]["env_vars"]
    else:
      env_vars = ""
    niters = iters
    if thread <= 32:
      niters /= 2
    env_vars = env_vars % { 'threads': thread }
    args = OPTIONS_BY_LIB[lib]["args"] % { 'n': n, 'bs': bs, 'iters': niters }
    numactl_text = "with_numactl" if numactl else "without_numactl"
    numactl_cmd = "numactl -i all" if numactl else ""
    cmd_template = OPTIONS_BY_LIB[lib]["command_template"]
    for prog_name in OPTIONS_BY_LIB[lib]["progs"]:
      prog_cmd = "%(path)s/%(exe)s %(args)s" % { 'exe': cmd_template % { 'prog': prog_name }, 'args': args, 'path': path }
      full_cmd = " >>".join([" ".join([env_vars, numactl_cmd, prog_cmd]), OUTFILE % {'date': DATETIME, 'arch': arch,'lib': lib, 'runtime': runtime, 'prog': prog_name, 'numactl': numactl_text}])
      commands.append(full_cmd)
  return commands


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="Runs several Chameleon versions on specific architectures")
  parser.add_argument("arch", help="Choose architectures to run on",
                      choices=["souris", "brise"])
  parser.add_argument("lib", help="Choose programs to run",
                      choices=["chameleon", "plasma"])
  parser.add_argument("--numactl", help="Use numactl or not",
                      choices=["yes", "no", "both"], default="both")
  parser.add_argument("--niter", help="Number of iterations for all cases",
                      type=int, default=5)
  options = parser.parse_args()

  numactl_runs = []

  if options.numactl == "no" or options.numactl == "both":
    numactl_runs.append(False)
  if options.numactl == "yes" or options.numactl == "both":
    numactl_runs.append(True)

  print "# Generating tests for threads"
  for numactl, thread, matrix in itertools.product(numactl_runs, OPTIONS_BY_ARCH[options.arch]["threads"], OPTIONS_BY_ARCH[options.arch]["matrix_sizes"]):
    n, bs = matrix
    for cmd in get_cmds_for(thread, options.lib, options.arch, n, bs, numactl, options.niter):
      print cmd
      os.system(cmd)
  print "# Generating tests for blocksizes"
  for numactl, matrix, bs, thread in itertools.product(numactl_runs, OPTIONS_BY_ARCH[options.arch]["matrix_sizes"], OPTIONS_BY_ARCH[options.arch]["blocksizes"], [96, 64, 32]):
    n, _ = matrix
    for cmd in get_cmds_for(thread, options.lib, options.arch, n, bs, numactl, options.niter):
      print cmd
      os.system(cmd)
