#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import re

# filename_regex=re.compile(r'^.*/(?P<date>[\d-:_]+)\.(?P<arch>\w+)\.(?P<lib>\w+).(?P<runtime>\w+).(?<numactl>\w+).log$')
filename_regex=re.compile(r'^(.*/)?(?P<date>[:\d_-]+)\.(?P<arch>\w+)\.(?P<lib>\w+)\.(?P<runtime>\w+(-\d\.\d\.\d)?)\.(?P<prog>\w+)\.(?P<numactl>\w+)\.log$')
repeated_pattern=re.compile(r'\s+(?P<size>[\d]+\s+)(?P=size){1,2}')
almost_repeated_pattern=re.compile(r'\s+(?P<size>[\d]+)\s+(?P<nb>\d+)\s+(?P=size)\s+')

line_format="%(lib)s %(arch)s %(numactl)s %(prog)s %(runtime)s %(line)s"

def get_traits_from_filename(filename):
  res = filename_regex.search(filename)
  if res is None:
    print "Filename '%s' wrongly formatted" % filename
    quit()
  return res.group("arch"), res.group("lib"), res.group("runtime"), res.group("prog"), res.group("numactl")

def change_line(lib, line, arch, runtime, prog, numactl):
  if lib == "chameleon":
    line = line.replace("+-", "")
    line = line.replace("  1  ", "  ")
  elif lib == "plasma":
    line = line.replace("--", "").replace("  ", " ").replace(" n ", " ")
    line = line.replace(" f ", "").replace(" l ", "").replace(" o ", "")
  line = re.sub(repeated_pattern, ' \g<size> ', line)
  line = re.sub(almost_repeated_pattern, ' \g<size> \g<nb> ', line)

  return line_format % {
      'lib': lib, 'arch': arch, 'runtime': runtime,
      'prog': prog, 'numactl': numactl, 'line': line
      }


def should_process_line(lib, line):
  doit = True
  if lib == "chameleon":
    #in some cases (execution is stopped), the line may not have a "\n" at the end
    doit = not line.startswith("#") and not line.endswith("#\n") and line.endswith("\n")
  elif lib == "plasma":
    doit = not "Status" in line and len(line) > 5
  return doit

def process(lib, filename, arch, runtime, prog, numactl, output):
  print "Processing %s log" % lib
  with open(filename, 'r') as log, open(output,'a') as data_file:
    data_file.writelines(change_line(lib, line, arch, runtime, prog, numactl) for line in log if should_process_line(lib, line))

def process_plasma(filename, arch, runtime, prog, numactl, output):
  print "Processing plasma log"

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="Post process several Chameleon and plasma logs")
  parser.add_argument("files", help="List of files",
                      nargs='+')
  parser.add_argument("--output", help="Output data file",
                      default="all.dat")

  options = parser.parse_args()
  # print options.files
  for filename in options.files:
    arch, lib, runtime, prog, numactl = get_traits_from_filename(filename)
    numactl = "yes" if numactl == "with_numactl" else "no"
    if lib != "chameleon" and lib != "plasma":
      print "Unrecognized lib %s" % lib
      quit()
    process(lib, filename, arch, runtime, prog, numactl, options.output)
