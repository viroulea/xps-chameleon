# A few experiments with Chameleon and Plasma OpenMP

## Setup of the various tools

### BLAS/LAPACK

Provided by the Intel's MKL 2018 update 2.

### GCC/libGOMP

GCC's version used in these experiments is 8.1.
The runtime used is the libGOMP shipped with it.

### StarPU

StarPU release 1.2.5 was used.

Getting StarPU and building+installing it to its own directory basically needs very few steps:

```bash
wget https://gforge.inria.fr/frs/download.php/file/37664/starpu-1.2.5.tar.gz
tar -xf starpu-1.2.5.tar.gz
cd starpu-1.2.5
mkdir build && cd build
../configure --enable-openmp --prefix=/home/pviroule/install-brise/starpu-125 CC=gcc CXX=g++ F77=gfortran FC=gfortran --enable-maxcpus=96
make install
```

### Chameleon

We used two builds of Chameleon: one using the StarPU implementation for the kernels, and one using the OpenMP implementation

Configuring Chameleon for StarPU was done the following way:

```bash
cmake .. -DCMAKE_BUILD_TYPE=Release -DCHAMELEON_USE_CUDA=OFF -DCHAMELEON_USE_MPI=OFF -DSTARPU_DIR=/home/pviroule/install-brise/starpu-125 -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_Fortran_COMPILER=gfortran
```

And for OpenMP:

```bash
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_Fortran_COMPILER=gfortran -DCHAMELEON_SCHED_OPENMP=on
```

Building everything will create the tests drivers in the `testing` directory.

### Plasma OpenMP

Plasma's repository has an example `make.inc.mkl-gcc` that can be used as a `make.inc` to configure Plasma to use gcc + Intel's MKL.
Once built, a generic test driver is available in the `test` directory.

## Running experiments

We tested 3 versions of linear algebra library: Chameleon+libGOMP, Chameleon+StarPU, Plasma OpenMP (via libGOMP).

The script `run.py` can be used to generate a shell script with a bunch of experiments for various sizes and number of threads.
By default it will generate all the test cases that were ran for these experiments.

It generally follows the same pattern for the 3 libraries:

Chameleon+libGOMP:

```bash
OMP_NUM_THREADS=$threads OMP_PLACES="{0}:$threads:1" ./time_dpotrf_tile -m $n -n $n -b $bs --niter=5
```

Plasma OpenMP:
```bash
OMP_NUM_THREADS=$threads OMP_PLACES="{0}:$threads:1" numactl -i all ./test dpotrf --dim=$n --nb=$bs
```

In both cases the threads are binded through `OMP_PLACES`.
We use `numactl` for Plasma OpenMP to mitigate their single-threaded data initialization.

Chameleon+StarPU:

```bash
STARPU_NCPU=$threads  ./time_dpotrf_tile -m $n -n $n -b $bs --niter=5
```


## Processing data

In most cases the library output has to be filtered to keep only the relevant lines (eg: grepping out the line with "#" in Chameleon's output).
Data for each library are kept in `logs-brise`, and running `Rscript gen-graphs-brise.R` will recreate the graphs.

