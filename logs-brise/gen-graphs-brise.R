library(ggplot2)
library(plyr)

frame_gcc_no_numactl = read.table("./data.chameleon.libgomp.log", header=FALSE)
frame_gcc_numactl = read.table("./data.chameleon.libgomp.with_numactl.log", header=FALSE)
frame_starpu_no_numactl = read.table("./data.chameleon.starpu.log", header=FALSE)
frame_starpu_numactl = read.table("./data.chameleon.starpu13.with_numactl.log", header=FALSE)
frame_plasma_no_numactl = read.table("./data.plasma-omp.libgomp.without_numactl.log", header=FALSE)
frame_plasma_numactl = read.table("./data.plasma-omp.libgomp.log", header=FALSE)

# NOTE: theoritical pic perf (gflops) for one core of Brise
max_gflops_per_core = 35.2
# NOTE: 5 is the hardcoded number of experiments. TODO: put this in output
# divider for the 95% confidence interval
divider = sqrt(5)

colnames(frame_gcc_no_numactl) = c("threads", "BS", "N", "M", "NRHS", "sec", "gflops", "stddev")
colnames(frame_gcc_numactl) = c("threads", "N", "M", "BS", "NRHS", "sec", "gflops", "stddev")
frame_gcc_numactl$runtime="libgomp"
frame_gcc_numactl$numactl="with_numactl"
frame_gcc_no_numactl$runtime="libgomp"
frame_gcc_no_numactl$numactl="without_numactl"
colnames(frame_starpu_no_numactl) = c("threads", "BS", "N", "M", "NRHS", "sec", "gflops", "stddev")
colnames(frame_starpu_numactl) = c("threads", "N", "M", "BS", "NRHS", "sec", "gflops", "stddev")
frame_starpu_no_numactl$runtime="starpu"
frame_starpu_no_numactl$numactl="without_numactl"
frame_starpu_numactl$runtime="starpu13"
frame_starpu_numactl$numactl="with_numactl"
colnames(frame_plasma_numactl) = c("threads", "sec", "gflops_", "not_used1", "N", "BS", "not_used2", "not_used3")
colnames(frame_plasma_no_numactl) = c("threads", "sec", "gflops_", "not_used1", "N", "BS", "not_used2", "not_used3")

frame_avg_plasma_numactl = ddply(frame_plasma_numactl, c("threads", "N", "BS"), summarize, gflops = mean(gflops_), stddev=sd(gflops_), Nxp=length(threads))
frame_avg_plasma_no_numactl = ddply(frame_plasma_no_numactl, c("threads", "N", "BS"), summarize, gflops = mean(gflops_), stddev=sd(gflops_), Nxp=length(threads))
frame_avg_plasma_numactl$runtime="plasma-omp"
frame_avg_plasma_numactl$numactl="with_numactl"
frame_avg_plasma_no_numactl$runtime="plasma-omp"
frame_avg_plasma_no_numactl$numactl="without_numactl"

common_columns=c("threads", "N", "BS", "gflops", "stddev", "runtime", "numactl")
frame_gcc_numactl = frame_gcc_numactl[common_columns]
frame_gcc_no_numactl = frame_gcc_no_numactl[common_columns]
frame_starpu_numactl = frame_starpu_numactl[common_columns]
frame_starpu_no_numactl = frame_starpu_no_numactl[common_columns]
frame_avg_plasma_numactl = frame_avg_plasma_numactl[common_columns]
frame_avg_plasma_no_numactl = frame_avg_plasma_no_numactl[common_columns]

full_frame = rbind(frame_gcc_numactl, frame_gcc_no_numactl, frame_starpu_numactl, frame_starpu_no_numactl, frame_avg_plasma_numactl, frame_avg_plasma_no_numactl)

pdf("evolution_vs_threads.pdf", width=18, height=6)

myplot = ggplot(subset(full_frame, BS==320), aes(x=threads, y=gflops))
myplot = myplot + geom_ribbon(aes(group=interaction(runtime, numactl), ymax=gflops+2*stddev/divider, ymin=gflops-2*stddev/divider), fill="grey80")
myplot = myplot + geom_line(aes(color=interaction(runtime, numactl), group=interaction(runtime, numactl)))
myplot = myplot + geom_abline(intercept=0, slope=max_gflops_per_core, colour="red")
myplot = myplot + facet_wrap(~N)

print(myplot)

dev.off()

pdf("evolution_vs_bs.pdf", width=18, height=6)

myplot = ggplot(subset(full_frame, threads==96), aes(x=BS, y=gflops))

myplot = myplot + geom_ribbon(aes(group=interaction(runtime, numactl), ymax=gflops+2*stddev/divider, ymin=gflops-2*stddev/divider), fill="grey80")
myplot = myplot + geom_line(aes(color=interaction(runtime, numactl), group=interaction(runtime, numactl)))
myplot = myplot + facet_wrap(~N)
print(myplot)

dev.off()

pdf("granularity.pdf", width=18, height=6)
data_for_plot = subset(full_frame, (N== 8192 | N== 32768) & threads==96 & ((runtime == "libgomp" & numactl == "without_numactl")
                                                  | (runtime == "starpu13" & numactl == "with_numactl")
                                                  | (runtime == "plasma-omp" & numactl == "with_numactl")))
myplot = ggplot(data_for_plot, aes(x=BS, y=gflops))
myplot = myplot + geom_ribbon(aes(group=runtime, ymax=gflops+2*stddev/divider, ymin=gflops-2*stddev/divider), fill="grey80")
myplot = myplot + geom_line(aes(color=runtime))
myplot = myplot + scale_color_discrete(name="Library", labels = c("Chameleon + libGOMP", "Plasma OMP + libGOMP", "Chameleon + StarPU 1.3"))
myplot = myplot + facet_wrap(~N)
myplot = myplot + xlab("Blocksize")
myplot = myplot + ylab("GFlops")
print(myplot)
dev.off()

pdf("scalability.pdf", width=18, height=10)
data_for_plot = subset(full_frame, (N==8192 | N==32768) & BS == 320 & ((runtime == "libgomp" & numactl == "without_numactl")
                                                  | (runtime == "starpu13" & numactl == "with_numactl")
                                                  | (runtime == "plasma-omp" & numactl == "with_numactl")))
myplot = ggplot(data_for_plot, aes(x=threads, y=gflops))
myplot = myplot + geom_ribbon(aes(group=runtime, ymax=gflops+2*stddev/divider, ymin=gflops-2*stddev/divider), fill="grey80")
myplot = myplot + geom_line(aes(color=runtime))
myplot = myplot + scale_color_discrete(name="Library", labels = c("Chameleon + libGOMP", "Plasma OMP + libGOMP", "Chameleon + StarPU 1.3"))
myplot = myplot + facet_wrap(~N)
myplot = myplot + xlab("Threads")
myplot = myplot + ylab("GFlops")
print(myplot)
dev.off()
